@extends('layouts.layout')
@section('content')
<div class='container'>
  
  <div class="col-md-10">

      <div class="col-md-0 top-10">
        <div class="col-md-12">

          <div class="panel">
            <div class="panel-body">
              <div>
    <h3>Edit Inventory</sh3>
  </div>
  <form action="{{ route('inventory.edit', $product->id) }}" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Product Name</label>
        <input type="text" name="name" id="name" value="{{ $product->name }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input type="number" step="0.01" name="price" id="price" value="{{ $product->price }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" name="quantity" id="quantity" value="{{ $product->quantity }}" class="form-control">
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-outline-primary">
        Update
      </button>
    </div>
  </form>
</div>
@endsection