@extends('layouts.layout')
@section('content')
@include('errors')
@include('success')
 
  <div class='container'>
  <div class="col-md-10">
   <div class="col-md-0 top-10">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-body">
              <div>
            <h3>Edit Inventory</h3>
          </div>
       
        <form method="post" action="{{ route('inventory.store') }}">
          @csrf
          <div class="form-group">
              <label for="name">Product Name</label>
              <input type="text" name="name" id="name" class="form-control">
          </div>
          <div class="form-group">
              <label for="price">Price</label>
              <input type="number" step="0.01" name="price" id="price" class="form-control">
          </div>
          <div class="form-group">
              <label for="quantity">Quantity</label>
              <input type="number" name="quantity" id="quantity" class="form-control">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">
              Save
            </button>
          </div>
  </form>
</div>
@endsection