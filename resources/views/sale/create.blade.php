@extends('layouts.layout')
@section('content')
@include('errors')
@include('success')


<div class='container'>
  <div class="col-md-10">
   <div class="col-md-0 top-10">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-body">
              <div>
    <h3>Add sales</h3>
  </div>
  <form method="post" action="{{ route('sale.create') }}">

    @csrf
    <div class="form-group">
        <label for="product_id">Select a product</label>
        <select name='product_id' class="form-control" id="product_id" >
          @foreach ($products as $product)
            <option value='{{$product->id}}'>{{$product->name}} - stock: {{$product->quantity}}</option>
          @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" name="quantity" id="quantity" class="form-control">
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-outline-primary">
        add sale
      </button>
    </div>
  </form>
</div>
@endsection


