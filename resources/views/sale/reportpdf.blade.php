<?php

use App\Sale;

$total = 0;

?>
<center>
	<h3>Sales report</h3>
	<br/>
</center>
<div class="header"></div>
<div class="detail">
	<table>
		<tr>
			<th>PS Mini Mart</th>
			<td></td>
		</tr>
	</table>
	<h4>Date Genarate: 
		<?php echo date("m/d/y");?>
	</h4>
</div>
<table border=1 style="width:100%">
	<tr>
		<th>Name</th>
		<th>Remaining</th>
		<th>Price</th>
		<th>Sold</th>
		<th>Earning</th>
	</tr>
	@foreach ($products as $product)
	
	<?php
		$total_product =  Sale::where('product_id', $product->id)->sum('quantity') * $product->price;
		$total += $total_product; 
	?>
	<tr>
		<td>{{ $product->name }}</td>
		<td>{{ $product->quantity }}</td>
		<td>RM {{ $product->price }}</td>
		<td>{{ Sale::where('product_id', $product->id)->sum('quantity') }}</td>
		<td>RM {{ $total_product }}</td>
	</tr>
	@endforeach

</table>
<h3>Total Sales: RM {{ $total }}</h3>