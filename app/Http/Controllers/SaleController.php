<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use App\Product;
use Session;
use PDF;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::orderBy('date', 'desc')->paginate(10);
        return view('sale.index', compact('sales'));
    }
    public function today() {
        $sales = Sale::where('date', date('Y-m-d'))->get();
        return view('sale.index', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::where('quantity', '>',  0)->get();
        return view('sale.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'product_id' => 'required',
        'quantity' => 'required',
      ]);
      $product_id = $request->input('product_id');
      $quantity = $request->input('quantity');
      $quantity = $request->input('quantity');
      $product = Product::find($product_id);
      if ($quantity > $product->quantity) {
          return redirect()->back()->withErrors('Asking quantity is more than stock');
      }
      $product->quantity = $product->quantity - $quantity;
      $product->save();
      Sale::create([
        'product_id' => $product_id,
        'quantity' => $quantity,
        'price' => $product->price,
        'date' => date('Y-m-d')
      ]);
      Session::flash('message', 'Sales record added successfully');
      return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Sale::find($id)->delete();
      Session::flash('message', 'One item delete from sales inventory');
      return redirect()->back();
    }

 public function report()
    {
        $products = Product::get();
        $pdf = PDF::loadView('sale.reportpdf', ['products' => $products]);
        return @$pdf->stream();
    }

}
   /* private function countTotalSale($sales)
    {
        //DEFAULT TOTAL BERNILAI 0
        $total = 0;
        //JIKA DATA ADA
        if ($sales->count() > 0) 
        {
            //MENGAMBIL VALUE DARI TOTAL -> PLUCK() AKAN MENGUBAHNYA MENJADI ARRAY
            $sub_total = $sales->pluck('price')->all();
            //KEMUDIAN DATA YANG ADA DIDALAM ARRAY DIJUMLAHKAN
            $total = array_sum($sub_total);
        }
        return $total;
    }
    ​
    private function countItem($sales)
    {
        //DEFAULT DATA 0
        $data = 0;
        //JIKA DATA TERSEDIA
        if ($sales->count() > 0) {
            //DI-LOOPING
            foreach ($sales as $row) {
                //UNTUK MENGAMBIL QTY 
                $quantity = $row->order_detail->pluck('quantity')->all();
                //KEMUDIAN QTY DIJUMLAHKAN
                $val = array_sum($qty);
                $data += $val;
            }
        } 
        return $data;
    }
    ​
    public function report($report)
    {
        //MENGAMBIL DATA TRANSAKSI BERDASARKAN INVOICE
        $sale = Sale::where('report', $report)
                ->with('id', 'name', 'quantity')->first();
        //SET CONFIG PDF MENGGUNAKAN FONT SANS-SERIF
        //DENGAN ME-LOAD VIEW INVOICE.BLADE.PHP
        $pdf = PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])
            ->loadView('orders.report.invoice', compact('order'));
        return $pdf->stream();
    }
    */
