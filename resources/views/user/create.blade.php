@extends('layouts.layout')
@section('content')
<div class='container'>
  
  <div class="col-md-10">

      <div class="col-md-0 top-10">
        <div class="col-md-12">

          <div class="panel">
            <div class="panel-body">
              <div>
    <h3>Add New Staff</h3>
  </div>
  <form action="{{ route('user.store') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control">
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-outline-primary">
        Update
      </button>
    </div>
  </form>
</div>
@endsection