<?php

use App\Product;

use App\Sale;

$totalStockIn = 0;
$totalStockOut = 0;
?>
<center>
    <h3>Inventory report</h3>
    <br/>
</center>
<div class="header"></div>
<div class="detail">
    <table>
        <tr>
            <th>PS Mini Mart</th>
        </tr>
    </table>
    <h4>Date Genarate: 
        <?php echo date("m/d/y");?>
    </h4>
</div>
<table border=1 style="width:100%">
    <tr>
        <th>Name</th>
        <th>Initial Stock</th>
        <th>Sold</th>
        <th>Remaining</th>
    </tr>

    @foreach ($products as $product)

    
    <?php
    $sold = Sale::where('product_id', $product->id)->sum('quantity');

    $totalStockIn += $product->quantity;
    $totalStockOut += $sold;
    ?>
    <tr>
        <td>{{ $product->name }}</td>
        <td>{{ $product->quantity }}</td>
        <td>{{ $sold }}</td>
        <td>{{ $product->quantity - $sold }}</td>
    </tr>
   
    @endforeach

<table border=0 style="width:100%">
    <tr>
        <th><h4>Total Stock In: {{ $totalStockIn }}</h4></th>
        <th><h4>Total Stock Out: {{ $totalStockOut }}</h4></th>
        <th><h4>Available Stock: {{ $totalStockIn - $totalStockOut }}</h4></th>
    </tr>
</table>
