@extends('layouts.layout')
@section('content')
  @include('success')

<div class='container'>
  
  <div class="col-md-10">

      <div class="col-md-0 top-10">
        <div class="col-md-12">

          <div class="panel">
            <div class="panel-body">
              <div>
              <h3>Manage Staff</h3>
            </div>
              <div class="col-md-12 padding-0" style="padding-bottom:20px;">
                <div class="col-md-6" style="padding-left:10px;">
                    </div>
                    <div class="col-md-6">
                    </div>
                 </div>
                  <div class="responsive-table">
                    <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        @foreach ($users as $user)
                          <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                         
                            <td>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                            <a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-primary">
                        Update
                        </a>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </tr>
                      <tr> 
                    </tbody>
                  </table>
                  </div>
                  <div class="col-md-6" style="padding-top:20px;">
                    <span>showing 0-10 of 30 items</span>
                  </div>
                  <div class="col-md-6">
                  </div>
                      <div class="col-md-6" style="padding-left:250px;">
                      <a href="{{ route('user.create') }}" class="btn btn-primary">Add User</a>                  
                </div>
</tr>
</tbody>

          <!-- end: content -->
 
@endsection
