<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/x', function() {
	echo '<form method="post" action="/z">name: <input name="name"/><button type="submit">ok</button></form>';
});

Route::get('/y', 'ProductController@yy');

Route::post('/z', 'ProductController@zz');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

    Route::get('/inventory/report', 'ProductController@report');


Route::group(['middleware' => 'auth'], function () {
  Route::resource('inventory', 'ProductController');

  Route::post('/inventory/{id}/edit', 'ProductController@update');
  Route::post('sale/today', 'SaleController@today')->name('sale.today');
  Route::resource('sales', 'SaleController', ['names' => ['destroy' => 'sale.destroy']]);
  Route::post('/sales/create', 'SaleController@store')->name('sale.create');
  Route::get('/sale/report', 'SaleController@report');

  Route::get('/', 'ProductController@index');


  Route::resource('user', 'StaffController');
  Route::post('/user/{id}/edit', 'StaffController@update');
  Route::get('/', 'StaffController@index');

  //Route::get('/report', 'ReportController@pdf');

});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout', 'Auth\LoginController@logout');


