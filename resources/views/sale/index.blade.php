@extends('layouts.layout')
@section('content')

<div class='container'>
  <div class="col-md-10">
    <div class="col-md-0 top-10">
      <div class="col-md-12">
        <div class="panel">
          <div class="panel-body">
            <div>
              <h3>Sale</h3>
            </div>

  @include('success')
  @if(count($sales))

  
            <table class='table table-bordered'>
              <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Total</th>
                <th>date</th>
                <th>Action</th>
              </tr>
              <tr>
      @foreach ($sales as $sale)
        
                <tr>
                  <td>{{$sale->product->name}}</td>
                  <td>{{$sale->quantity}}</td>
                  <td>{{$sale->price}}</td>
                  <td>{{$sale->total}}</td>
                  <td>{{$sale->date}}</td>
                  <td>
                    <form action='{{ route('sale.destroy', ['sale' => $sale->id]) }}' class="d-inline" method="post">
              @csrf
              @method('delete')
              
            </form>
                  </td>
                </tr>
      @endforeach
    
              </tr>
            </table>
  {{$sales->links()}}
  
            <ul class="pagination pull-right">
              <a href="{{ route('sales.create') }}" class="btn btn-primary">Add Sales</a>
            </ul>

  @else 
  
            <div class='p-5 text-white bg-danger mb-3'>
              <h3>Look Like You don't have sales!</h3>
            </div>
  @endif

          </div>
@endsection















