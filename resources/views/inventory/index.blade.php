@extends('layouts.layout')
@section('content')
@include('success')

<div class='container'>
  <div class="col-md-10">
    <div class="col-md-0 top-10">
        <div class="panel">
          <div class="panel-body">
            <div>
              <h3>Inventory</h3>
            </div>
            <div class="col-md-12 padding-0" style="padding-bottom:20px;">
              <div class="col-md-6" style="padding-left:10px;"></div>
              <div class="col-md-6">
                <div class="col-lg-12">
                  <div class="input-group"></div>
                  <!-- /btn-group -->
                </div>
                <!-- /input-group -->
              </div>
              <!-- /.col-lg-6 -->
            </div>
          </div>
          <div class="responsive-table">
            <table class="table table-striped table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Inventory</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                        @foreach ($products as $product)
                          
                  <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>
                      <form action='{{ route('inventory.destroy', ['inventory' => $product->id]) }}' class="d-inline" method="post" onsubmit = "return confirm('Are you sure?')">
                        <input type="hidden" name="_method" value="DELETE">

                                @csrf
                                @method('delete')
                                
                          <button type="submit" class="btn btn-danger">Delete</button>
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <a href="{{ route('inventory.edit', ['id' => $product->id]) }}" class="btn btn-primary">
                        Update
                        </a>
                          </form>
                        </td>
                      </tr>
                        @endforeach
                      
                    </tr>
                    <tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-6" style="padding-top:20px;">
                  <span>showing 0-10 of 30 items</span>
                </div>
                <div class="col-md-6">
                  <ul class="pagination pull-right">
                    <a href="{{ route('inventory.create') }}" class="btn btn-primary">Add Item</a>
                          {{$products->links()}}
                        
                  </ul>
                </div>
                      
                      {{-- 
                <a href="{{ route('inventory.edit.index')}}" class="btn btn-primary">Edit Item</a> --}}
                      
                
              </div>
              <!-- end: content -->
 
@endsection
