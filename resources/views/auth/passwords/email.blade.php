@extends('layouts.loginlayout')

@section('content')

    <form method="POST" class="form-signin" action="{{ route('password.email') }}">
        @csrf
          <div class="panel periodic-login">
              <div class="panel-body text-center">
                  <h1 class="element-name">Mini Market Management System</h1>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="email" class="form-text" name="email" required>
                    <span class="bar"></span>
                    <label>Email</label>
                  </div>
                  <input type="submit" class="btn col-md-12" value="Reset password"/>
              </div>
                <div class="text-center" style="padding:5px;">
                    <a href="{{ route('login') }}">Login </a>
                    <a href="{{ route('register') }}">| Signup</a>
                </div>
          </div>
        </form>

@endsection
