@extends('layouts.loginlayout')
@section('content')
        <form class="form-signin" method="post">
            @csrf
          <div class="panel periodic-login">
              <div class="panel-body text-center">
                  <h1 class="element-name">Mini Market Management System</h1>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="email" class="form-text" name="email" required>
                    <span class="bar"></span>
                    <label>Email</label>
                  </div>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="password" class="form-text" name="password" required>
                    <span class="bar"></span>
                    <label>Password</label>
                  </div>
                  <label class="pull-left">
                  <input type="checkbox" class="icheck pull-left" name="checkbox1"/> Remember me
                  </label>
                  <input type="submit" class="btn col-md-12" value="SignIn"/>
              </div>
                <div class="text-center" style="padding:5px;">
                    <a href="{{ route('password.request') }}">Forgot Password </a>
                    <a href="{{ route('register') }}">| Signup</a>
                </div>
          </div>
        </form>
@endsection